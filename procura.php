<!--
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
!-->
<!doctype html>
<html>
	<head>
		<title>Procurar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Pesquisar nos termos</h1>
		<form id="pesquisarTermos" name="pesquisarTermos" method="post" action="procurarTermos.php?todDev=0">
			<p>Procurar por: <select id="tipoProcura" name="tipoProcura">
				<option value="numTermo">Nº do termo</option>
				<option value="empresa">Empresa</option>
				<option value="nome">Nome</option>
				<option value="usuarioAD">Usuário do AD</option>
				<option value="cpf">CPF</option>
				<option value="equip">Equipamento</option>
			</select></p>
			<label>Procura: <input type="text" name="procura" id="procura" required /></label><br/>
			<label><input type="checkbox" name="palavraCompleta" id="palavraCompleta" /> Procurar pela palavra completa</label><br/>
			<label><input type="checkbox" name="jaDevolvido" id="jaDevolvido" /> Procurar apenas por termos já devolvidos</label><br/>
			<input type="submit" value="Procurar"/><br/>
			<a href="procurarTermos.php?todDev=1">Ver todos os termos já devolvidos</a>
		</form>
		<hr/>
		<h1>Ver termo</h1>
		<form id="verTermo" name="verTermo" method="get" action="termo.php">
			<p>Número do termo: <input type="text" name="numTermo" id="numTermo" size="15" required /><br/>
			<label><input type="radio" id="isDevol" name="isDevol" value="0" required />Termo de Responsabilidade</label><br/>
			<label><input type="radio" id="isDevol" name="isDevol" value="1"/>Termo de Devolução</label><br/>
			<!--<input type="hidden" name="isDevol" id="isDevolv" value="0"/>!-->
			<input type="submit" value="Pesquisar"/></p>
		</form>
		<?php include "rod.php";?>
	</body>
</html>
