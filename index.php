<!--
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
!-->
<!doctype html>
<html>
	<head>
		<title>Gerar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Gerador de termo</h1>
		<form id="gerarTermo" name="gerarTermo" method="get" action="gerarTermo.php">
			<p>Quantos campos serão necessários? <input type="number" name="quantCampos" id="quantCampos" size="15" maxLength="1" required /> </p>
			
			<input type="submit" value="Gerar Termo" />
		</form>
		<?php include "rod.php";?>
	</body>
</html>
