<?php
/*
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
*/
// Recebe a variável para pesquisar por TODOS termos devolvidos
$verTodosDevolvidos = $_GET['todDev'];

if($verTodosDevolvidos == 0){
	//Recebendo dados
	$tipoProcura = $_POST['tipoProcura'];
	$procura = $_POST['procura'];
	$palavraCompleta = false;
	if(isset($_POST['palavraCompleta'])){
		$palavraCompleta = true;
	}
	$jaDevolvido = 0;
	if(isset($_POST['jaDevolvido'])){
		$jaDevolvido = 1;
	}
}

include "funcao.php";

function imprimeAnexos($numTermo){
	$resultadoAnexos = ProcurarAnexo($numTermo);

	if ($resultadoAnexos->num_rows > 0) {
		$anexos = "";
		while($linha = $resultadoAnexos->fetch_assoc()) {
			$anexos = $anexos . '<a href="uploads/' . $linha["nomeAnexo"] . '" target=_blank>' . $linha["nomeAnexo"] . '</a> | ';
		}
	} else {
		$anexos = "Não há nenhum anexo.";
	}
	return $anexos;
}

?>
<!doctype html>
<html>
	<head>
		<title>Pesquisar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Pesquisar nos termos</h1>
		
		<?php
			if($verTodosDevolvidos == 0){
				// Pesquisa por termos
				$resultadoProcura = ProcurarTermo($palavraCompleta,$tipoProcura,$procura,$jaDevolvido); // Realiza a procura no banco
			} else {
				// Pesquisa por TODOS termos devolvidos
				$resultadoProcura = ProcurarTermo(false,"","",2); // O único valor importante é o "2"
			}
			
			if ($resultadoProcura->num_rows > 0) {
				if($verTodosDevolvidos == 0 and $jaDevolvido == 0){
					echo "<p>Termos que possuem o termo '" . $procura . "' no campo '" . $tipoProcura . "':</p>";
				} else if($verTodosDevolvidos == 0 and $jaDevolvido == 1){
					echo "<p>Termos devolvidos que possuem o termo '" . $procura . "' no campo '" . $tipoProcura . "':</p>";
				} else {
					echo "<p>Termos marcados como devolvidos:</p>";
				}
				?>
				<table width="100%" border="1">
					<tr>
						<td width="5%" class="cabecalho">Nº Termo</td>
						<td width="5%" class="cabecalho">Devolução</td>
						<td width="30%" class="cabecalho">Nome Completo</td>
						<td width="60%" class="cabecalho">Anexos</td>
					</tr>
					<?php
					while($linha = $resultadoProcura->fetch_assoc()){
						echo "<tr>";
						echo "<td width='5%'><a href='termo.php?numTermo=" . $linha["numTermo"] . "&isDevol=0' target=_blank>" . $linha["numTermo"] . "</a></td>";
						echo "<td width='5%'><a href='termo.php?numTermo=" . $linha["numTermo"] . "&isDevol=1' target=_blank>Gerar</a></td>";
						echo "<td width='30%'>" . $linha["nome"] . "</td>";
						echo "<td width='60%'>" . imprimeAnexos($linha["numTermo"]) . "</td>";
						echo "</tr>";
					}
					?>
				</table>
				<?php
			} else {
				print("Nenhum termo encontrado!");
			}
			
		?>
		
		<?php include "rod.php";?>
	</body>
</html>
