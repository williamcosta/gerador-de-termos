<!--
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
!-->
<?php
	//Recebendo dados
	$quantCampos = $_GET['quantCampos'];
?>
<!doctype html>
<html>
	<head>
		<title>Gerar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Gerador de termo</h1>
		<?php if($quantCampos > 0 and $quantCampos < 9):?>
			<form id="gerarTermo" name="gerarTermo" method="post" action="cadastrarTermo.php">
				<input type="hidden" name="quantCampos" id="quantCampos" value="<?php echo $quantCampos;?>"/>
				<p>Empresa: <input type="text" name="empresa" id="empresa" size="50" maxLength="60" required /> </p>
				<p>Nome completo: <input type="text" name="nome" id="nome" size="75" maxLength="120" required /> </p>
				<p>Usuário do AD: <input type="text" name="usuarioAD" id="usuarioAD" size="75" maxLength="120" required /> </p>
				<p>CPF: <input type="text" name="cpf" id="cpf" size="50" maxLength="11" required /> </p>
				
				<p>Tipo de termo:</p>
				<label><input type="radio" name="tipoTermo" value="norm" required />Termo de Responsabilidade</label><br/>
				<label><input type="radio" name="tipoTermo" value="empr"/>Termo de Empréstimo</label><br/><br/>
				<br/>
				<table width="100%" border="1">
					<tr>
						<td width="8%"><p>Cabeçalho</p></td>
						<?php for($i = 0;$i < $quantCampos;$i++){?>
							<td><input type="text" name="cab<?php echo $i+1;?>" id="cab<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(256,$quantCampos)-$quantCampos;?>" required /></td>
						<?php } ?>
					</tr>
					<tr>
						<td width="8%"><p>Equipamento 1</p></td>
						<?php for($i = 0;$i < $quantCampos;$i++){?>
							<td><input type="text" name="equip1_<?php echo $i+1;?>" id="equip1_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,$quantCampos)-$quantCampos;?>" required /></td>
						<?php } ?>
					</tr>
					<tr>
						<td width="8%"><p>Equipamento 2</p></td>
						<?php for($i = 0;$i < $quantCampos;$i++){?>
							<td><input type="text" name="equip2_<?php echo $i+1;?>" id="equip2_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,$quantCampos)-$quantCampos;?>" /></td>
						<?php } ?>
					</tr>
					<tr>
						<td width="8%"><p>Equipamento 3</p></td>
						<?php for($i = 0;$i < $quantCampos;$i++){?>
							<td><input type="text" name="equip3_<?php echo $i+1;?>" id="equip3_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,$quantCampos)-$quantCampos;?>" /></td>
						<?php } ?>
					</tr>
					<tr>
						<td width="8%"><p>Equipamento 4</p></td>
						<?php for($i = 0;$i < $quantCampos;$i++){?>
							<td><input type="text" name="equip4_<?php echo $i+1;?>" id="equip4_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,$quantCampos)-$quantCampos;?>" /></td>
						<?php } ?>
					</tr>
					<tr>
						<td width="8%"><p>Equipamento 5</p></td>
						<?php for($i = 0;$i < $quantCampos;$i++){?>
							<td><input type="text" name="equip5_<?php echo $i+1;?>" id="equip5_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,$quantCampos)-$quantCampos;?>" /></td>
						<?php } ?>
					</tr>
					<tr>
						<td width="8%"><p>Equipamento 6</p></td>
						<?php for($i = 0;$i < $quantCampos;$i++){?>
							<td><input type="text" name="equip6_<?php echo $i+1;?>" id="equip6_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,$quantCampos)-$quantCampos;?>" /></td>
						<?php } ?>
					</tr>
				</table>
				<!--
				<p>Campos da tabela: <input type="text" name="camposTabela" id="camposTabela" size="150" maxLength="256" required /> </p>
				<p>Equipamento 1: <input type="text" name="equip1" id="equip1" size="150" maxLength="280" required /> </p>
				<p>Equipamento 2: <input type="text" name="equip2" id="equip2" size="150" maxLength="280" /> </p>
				<p>Equipamento 3: <input type="text" name="equip3" id="equip3" size="150" maxLength="280" /> </p>
				<p>Equipamento 4: <input type="text" name="equip4" id="equip4" size="150" maxLength="280" /> </p>
				<p>Equipamento 5: <input type="text" name="equip5" id="equip5" size="150" maxLength="280" /> </p>
				<p>Equipamento 6: <input type="text" name="equip6" id="equip6" size="150" maxLength="280" /> </p>
				
				!-->
				<p>Descrição por extenso: <input type="text" name="descricao" id="descricao" size="150" maxLength="360" required /> </p>
				<br/>
				
				
				<input type="submit" name="enviar" id="enviar" value="Enviar dados" />
			</form>
		<?php else: ?>
			<h3>Erro - o número de colunas deve ser entre 1 e 8!</h3>
			<p>Clique <a href="index.php">aqui</a> para voltar e digite outro valor!</p>
		<?php endif; ?>
		<?php include "rod.php";?>
	</body>
</html>
