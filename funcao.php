<?php
/*
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
*/
	function OpenCon(){
		$dbhost = "localhost";
		$dbuser = "usuario";
		$dbpass = "123";
		$db = "sekki";
		$conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);

		return $conn;
	}
	
	function Inserir($empresa,$nomeCompleto,$cpf,$camposTabela,$equip1,$equip2,$equip3,$equip4,$equip5,$equip6,$descricao,$data,$usuarioAD,$tipoTermo){
		// Check connection
		$conn = OpenCon();
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}

		$sql = "INSERT INTO termos (empresa,nome,cpf,camposTabela,equipamento1,equipamento2,equipamento3,equipamento4,equipamento5,equipamento6,descricao,data,usuarioAD,tipoTermo,jaDevolvido) VALUE ('$empresa','$nomeCompleto','$cpf','$camposTabela','$equip1','$equip2','$equip3','$equip4','$equip5','$equip6','$descricao','$data','$usuarioAD','$tipoTermo',0)";

		if ($conn->query($sql) === TRUE) {
		  echo 'O termo foi gerado!<br/> Clique <a href="termo.php?numTermo=' . UltimoTermo() . '&isDevol=0" target=_blank >aqui</a> para visualizar seu termo.';
		} else {
		  echo "Houve um erro ao gerar o termo: " . $sql . "<br/>" . $conn->error;
		}

		$conn->close();
	}
	
	function EditarTermo($numTermo,$empresa,$nomeCompleto,$cpf,$camposTabela,$equip1,$equip2,$equip3,$equip4,$equip5,$equip6,$descricao,$data,$usuarioAD,$tipoTermo,$jaDevolvido){
		// Check connection
		$conn = OpenCon();
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}

		$sql = "UPDATE termos SET empresa='$empresa',nome='$nomeCompleto',cpf='$cpf',camposTabela='$camposTabela',equipamento1='$equip1',equipamento2='$equip2',equipamento3='$equip3',equipamento4='$equip4',equipamento5='$equip5',equipamento6='$equip6',descricao='$descricao',data='$data',usuarioAD='$usuarioAD',tipoTermo='$tipoTermo',jaDevolvido='$jaDevolvido' WHERE numTermo=$numTermo";

		if ($conn->query($sql) === TRUE) {
		  echo 'O termo foi editado com sucesso!<br/> Clique <a href="termo.php?numTermo=' . $numTermo . '&isDevol=0" target=_blank>aqui</a> para visualizar o termo atualizado.';
		} else {
		  echo "Houve um erro ao editar o termo: " . $sql . "<br/>" . $conn->error;
		}

		$conn->close();
	}
	
	function LerTermo($numTermo,$somenteBusca){
		// Create connection
		$conn = OpenCon();
		// Check connection
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}

		$sql = "SELECT * FROM termos WHERE numTermo='" . $numTermo . "'";
		$result = $conn->query($sql);
		
		$conn->close();
		
		if($somenteBusca){
			if ($result->num_rows > 0) {
				return "1 resultado";
			} else {
				return "0 resultados";
			}
		} else {
			if ($result->num_rows > 0) {
				return $result;
			} else {
				return "0 resultados";
			}
		}
	}
	
	function ProcurarTermo($isPalavraCompleta,$campo,$busca,$jaDevolvido){
		// Create connection
		$conn = OpenCon();
		// Check connection
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}
		
		if($jaDevolvido == 0){
			if($isPalavraCompleta AND $campo != "equip"){
				// -> Pesquisar por palavra completa e não é por equipamento
				$sql = "SELECT * FROM termos WHERE " . $campo . "='" . $busca . "'";
			} else if(!$isPalavraCompleta AND $campo != "equip") {
				// -> Pesquisar por palavra INcompleta e não é por equipamento
				$sql = "SELECT * FROM termos WHERE " . $campo . " LIKE '%" . $busca . "%'";
			} else {
				// -> Pesquisar por equipamentos
				$sql = "SELECT * FROM termos WHERE ((equipamento1 LIKE '%" . $busca . "%') OR (equipamento2 LIKE '%" . $busca . "%') OR (equipamento3 LIKE '%" . $busca . "%') OR (equipamento4 LIKE '%" . $busca . "%') OR (equipamento5 LIKE '%" . $busca . "%') OR (equipamento6 LIKE '%" . $busca . "%'))";
			}
		} else if($jaDevolvido == 1){
			if($isPalavraCompleta AND $campo != "equip"){
				// -> Pesquisar por palavra completa e não é por equipamento
				$sql = "SELECT * FROM termos WHERE " . $campo . "='" . $busca . "' AND jaDevolvido=1";
			} else if(!$isPalavraCompleta AND $campo != "equip") {
				// -> Pesquisar por palavra INcompleta e não é por equipamento
				$sql = "SELECT * FROM termos WHERE " . $campo . " LIKE '%" . $busca . "%' AND jaDevolvido=1";
			} else {
				// -> Pesquisar por equipamentos
				$sql = "SELECT * FROM termos WHERE jaDevolvido=1 AND ((equipamento1 LIKE '%" . $busca . "%') OR (equipamento2 LIKE '%" . $busca . "%') OR (equipamento3 LIKE '%" . $busca . "%') OR (equipamento4 LIKE '%" . $busca . "%') OR (equipamento5 LIKE '%" . $busca . "%') OR (equipamento6 LIKE '%" . $busca . "%'))";
			}
		} else {
			$sql = "SELECT * FROM termos WHERE jaDevolvido=1";
		}
		$result = $conn->query($sql);
		
		$conn->close();
		
		return $result;
		//return $sql;
	}
	
	function UltimoTermo(){
		// Create connection
		$conn = OpenCon();
		// Check connection
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}

		$sql = "SELECT numTermo FROM termos ORDER BY numTermo DESC LIMIT 1";
		$result = $conn->query($sql);
		
		$conn->close();
		
		return $result->fetch_assoc()["numTermo"];
	}
	
	function AnexarArquivo($anexo,$numTermo){
		// Check connection
		$conn = OpenCon();
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}

		$sql = "INSERT INTO anexos (nomeAnexo,termo) VALUE ('$anexo','$numTermo')";

		if ($conn->query($sql) === TRUE) {
		  print("O arquivo foi anexado ao termo");
		} else {
		  print("Houve um erro ao anexar o arquivo ao termo no banco de dados!<br/>" . $sql . "<br/>" . $conn->error);
		}

		$conn->close();
	}
	
	function ProcurarAnexo($numTermo){
		// Create connection
		$conn = OpenCon();
		// Check connection
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}
		
		$sql = "SELECT nomeAnexo FROM anexos WHERE termo=" . $numTermo;
		$result = $conn->query($sql);
		
		$conn->close();
		
		return $result;
	}
	
	function retornaNumeroAnexos($numTermo){
		// Create connection
		$conn = OpenCon();
		// Check connection
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}
		
		$sql = "SELECT nomeAnexo FROM anexos WHERE termo=" . $numTermo;
		$result = $conn->query($sql);
		
		$conn->close();
		
		return $result->num_rows;
	}
	
	function removeAnexo($nomeAnexo){
		// Create connection
		$conn = OpenCon();
		// Check connection
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}
		
		$sql = "DELETE FROM anexos WHERE nomeAnexo='" . $nomeAnexo . "'";
		if ($conn->query($sql) === TRUE) {
		  print("<br/>O anexo '" . $nomeAnexo . "' foi excluido com sucesso!");
		} else {
		  print("Houve um erro ao excluir o anexo do banco de dados!<br/>" . $sql . "<br/>" . $conn->error);
		}

		$conn->close();
	}
	
	function setDevolvido($numTermo){
		// Check connection
		$conn = OpenCon();
		if ($conn->connect_error) {
		  die("Conexão com o banco de dados falhou: " . $conn->connect_error);
		}

		$sql = "UPDATE termos SET jaDevolvido=1 WHERE numTermo=$numTermo";

		if ($conn->query($sql) === TRUE) {
		  echo 'O termo foi marcado como devolvido.';
		} else {
		  echo "Houve um erro ao marcar o termo como devolvido: " . $sql . "<br/>" . $conn->error;
		}

		$conn->close();
	}
?>
