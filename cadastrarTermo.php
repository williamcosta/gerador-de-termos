<?php
/*Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE*/

// Recebendo dados fixos
$empresa = $_POST['empresa'];
$nome = $_POST['nome'];
$cpf = $_POST['cpf'];
$descricao = $_POST['descricao'];
$data = date("d/m/Y");
$usuarioAD = $_POST['usuarioAD'];
$tipoTermo = $_POST['tipoTermo'];
$quantCampos = intval($_POST['quantCampos']);

// Recebendo dados variáveis

// CABEÇALHO - OBRIGATÓRIO
$camposTabela = "";
for($i = 1; $i <= $quantCampos; $i++){
	$camposTabela = $camposTabela . $_POST["cab" . $i];
	if($i < $quantCampos){
		$camposTabela = $camposTabela . ";";
	}
}

// EQUIPAMENTO 1 - OBRIGATÓRIO
$equipamento1 = "";
for($i = 1; $i <= $quantCampos; $i++){
	$equipamento1 = $equipamento1 . $_POST["equip1_" . $i];
	if($i < $quantCampos){
		$equipamento1 = $equipamento1 . ";";
	}
}

// EQUIPAMENTO 2
$equipamento2 = "";
if($_POST["equip2_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento2 = $equipamento2 . $_POST["equip2_" . $i];
		if($i < $quantCampos){
			$equipamento2 = $equipamento2 . ";";
		}
	}
}

// EQUIPAMENTO 3
$equipamento3 = "";
if($_POST["equip3_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento3 = $equipamento3 . $_POST["equip3_" . $i];
		if($i < $quantCampos){
			$equipamento3 = $equipamento3 . ";";
		}
	}
}

// EQUIPAMENTO 4
$equipamento4 = "";
if($_POST["equip4_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento4 = $equipamento4 . $_POST["equip4_" . $i];
		if($i < $quantCampos){
			$equipamento4 = $equipamento4 . ";";
		}
	}
}

// EQUIPAMENTO 5
$equipamento5 = "";
if($_POST["equip5_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento5 = $equipamento5 . $_POST["equip5_" . $i];
		if($i < $quantCampos){
			$equipamento5 = $equipamento5 . ";";
		}
	}
}

// EQUIPAMENTO 6
$equipamento6 = "";
if($_POST["equip6_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento6 = $equipamento6 . $_POST["equip6_" . $i];
		if($i < $quantCampos){
			$equipamento6 = $equipamento6 . ";";
		}
	}
}

?>
<!doctype html>
<html>
	<head>
		<title>Gerar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Gerador de termo</h1>
		<?php
			include "funcao.php";
			
			Inserir($empresa,$nome,$cpf,$camposTabela,$equipamento1,$equipamento2,$equipamento3,$equipamento4,$equipamento5,$equipamento6,$descricao,$data,$usuarioAD,$tipoTermo);
		?>
		<?php include "rod.php";?>
	</body>
</html>
