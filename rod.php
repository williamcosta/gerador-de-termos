<?php
/*
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
*/?>
<div class="rodape">
	<p>
	<b>Gerador de Termos 1.2.1</b><br/>
	© 2020 - Desenvolvido por: <a href="https://gitlab.com/williamcosta" target=_blank>wcosta</a><br/>
	Este código está licenciado sob a Licença de Três Cláusulas BSD - disponível <a href="https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE" target=_blank>aqui</a>
	</p>
</div>
