<?php
/*
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
*/

// Recebendo dados fixos
$numTermo = $_POST['numTermo'];
$empresa = $_POST['empresa'];
$nome = $_POST['nome'];
$usuarioAD = $_POST['usuarioAD'];
$cpf = $_POST['cpf'];
$descricao = $_POST['descricao'];
$data = $_POST[('data')];
$tipoTermo = $_POST['tipoTermo'];
$quantCampos = $_POST['quantCampos'];
$jaDevolvido = 0;
if(isset($_POST['jaDevolvido'])){
	$jaDevolvido = 1;
}

// Recebendo dados variáveis
// CABEÇALHO - OBRIGATÓRIO
$camposTabela = "";
for($i = 1; $i <= $quantCampos; $i++){
	$camposTabela = $camposTabela . $_POST["cab" . $i];
	if($i < $quantCampos){
		$camposTabela = $camposTabela . ";";
	}
}

// EQUIPAMENTO 1 - OBRIGATÓRIO
$equipamento1 = "";
for($i = 1; $i <= $quantCampos; $i++){
	$equipamento1 = $equipamento1 . $_POST["equip1_" . $i];
	if($i < $quantCampos){
		$equipamento1 = $equipamento1 . ";";
	}
}

// EQUIPAMENTO 2
$equipamento2 = "";
if($_POST["equip2_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento2 = $equipamento2 . $_POST["equip2_" . $i];
		if($i < $quantCampos){
			$equipamento2 = $equipamento2 . ";";
		}
	}
}

// EQUIPAMENTO 3
$equipamento3 = "";
if($_POST["equip3_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento3 = $equipamento3 . $_POST["equip3_" . $i];
		if($i < $quantCampos){
			$equipamento3 = $equipamento3 . ";";
		}
	}
}

// EQUIPAMENTO 4
$equipamento4 = "";
if($_POST["equip4_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento4 = $equipamento4 . $_POST["equip4_" . $i];
		if($i < $quantCampos){
			$equipamento4 = $equipamento4 . ";";
		}
	}
}

// EQUIPAMENTO 5
$equipamento5 = "";
if($_POST["equip5_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento5 = $equipamento5 . $_POST["equip5_" . $i];
		if($i < $quantCampos){
			$equipamento5 = $equipamento5 . ";";
		}
	}
}

// EQUIPAMENTO 6
$equipamento6 = "";
if($_POST["equip6_1"] != ""){
	for($i = 1; $i <= $quantCampos; $i++){
		$equipamento6 = $equipamento6 . $_POST["equip6_" . $i];
		if($i < $quantCampos){
			$equipamento6 = $equipamento6 . ";";
		}
	}
}

// Recebendo Anexos
$anexo1 = "";
$anexo2 = "";
$anexo3 = "";

if(isset($_POST['anexo1'])){
	$anexo1 = $_POST[('nomeAnexo1')];
}

if(isset($_POST['anexo2'])){
	$anexo2 = $_POST[('nomeAnexo2')];
}

if(isset($_POST['anexo3'])){
	$anexo3 = $_POST[('nomeAnexo3')];
}
?>
<!doctype html>
<html>
	<head>
		<title>Editar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Editar Termo</h1>
		<?php
			include "funcao.php";
			
			EditarTermo($numTermo,$empresa,$nome,$cpf,$camposTabela,$equipamento1,$equipamento2,$equipamento3,$equipamento4,$equipamento5,$equipamento6,$descricao,$data,$usuarioAD,$tipoTermo,$jaDevolvido);
			
			if($anexo1 != ""){
				if(unlink("uploads/" . $anexo1)){
					removeAnexo($anexo1);
				} else {
					echo "<br/>Erro ao deletar o arquivo!";
				}
			}
			if($anexo2 != ""){
				if(unlink("uploads/" . $anexo2)){
					removeAnexo($anexo2);
				} else {
					echo "<br/>Erro ao deletar o arquivo!";
				}
			}
			if($anexo3 != ""){
				if(unlink("uploads/" . $anexo3)){
					removeAnexo($anexo3);
				} else {
					echo "<br/>Erro ao deletar o arquivo!";
				}
			}
		?>
		<?php include "rod.php";?>
	</body>
</html>
