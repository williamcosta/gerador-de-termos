<?php
/*
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
*/?>
<ul class="menu">
  <li class="linhaMenu"><a class="linkMenu" href="index.php">Gerador de termos</a></li>
  <li class="linhaMenu"><a class="linkMenu" href="procura.php">Procurar por termos</a></li>
  <li class="linhaMenu"><a class="linkMenu" href="anexar.php">Anexar arquivo</a></li>
  <li class="linhaMenu"><a class="linkMenu" href="editar.php">Editar termo</a></li>
</ul>
