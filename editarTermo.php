<?php
	/*
	Gerador de Termos 1.2.1

	Copyright © 2020, William Alcântara Costa
	Todos os direitos reservados.
	Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
	*/
	//Recebendo dados
	$numTermo = $_GET['numTermo'];
	
	include "funcao.php";
	
	//$numeroAnexos = 0;
	
	function verificarAnexos($numTermo){		
		$resultadoAnexos = ProcurarAnexo($numTermo);

		$numeroAnexos = 0;
		if ($resultadoAnexos->num_rows > 0) {
			while($linha = $resultadoAnexos->fetch_assoc()) {
				$numeroAnexos++;
				echo "<label><input type='checkbox' name='anexo" . strval($numeroAnexos) . "' />" . $linha['nomeAnexo'] . "</label><br/>";
				echo "<input type='hidden' name='nomeAnexo" . strval($numeroAnexos) . "' value='" . $linha['nomeAnexo'] . "'/>";
			}
		} else {
			echo "<p>Não há nenhum anexo nesse termo!</p>";
		}
	}
		
	// COMEÇO DA PÁGINA EDITARTERMO.PHP
	
	if(strcmp(LerTermo($numTermo,true),"0 resultados")){
		// TERMO ENCONTRADO
		
		$row = LerTermo($numTermo,false)->fetch_assoc();
		$empresa = $row["empresa"];
		$nome = $row["nome"];
		$usuarioAD = $row["usuarioAD"];
		$cpf = $row["cpf"];
		$camposTabela = explode(";",$row['camposTabela']); // count($camposTabela) serve como quantCampos
		$equipamento1 = explode(";",$row['equipamento1']);
		$equipamento2 = explode(";",$row['equipamento2']);
		$equipamento3 = explode(";",$row['equipamento3']);
		$equipamento4 = explode(";",$row['equipamento4']);
		$equipamento5 = explode(";",$row['equipamento5']);
		$equipamento6 = explode(";",$row['equipamento6']);
		$descricao = $row['descricao'];
		$data = $row['data'];
		$tipoTermo = $row['tipoTermo'];
		$jaDevolvido = $row['jaDevolvido'];
		
		
		$termoEncontrado = true;
	} else {
		// TERMO **NÃO** ENCONTRADO
		
		$termoEncontrado = false;
	}
?>
<!doctype html>
<html>
	<head>
		<title>Editar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Editar Termo</h1>
		
		<?php if($termoEncontrado):?>
		<form id="gerarTermo" name="gerarTermo" method="post" action="editarTermo_pro.php">
			<p>Termo Nº <?php print($numTermo);?></p>
			<input type="hidden" id="numTermo" name="numTermo" value="<?php echo $numTermo;?>">
			<input type="hidden" id="quantCampos" name="quantCampos" value="<?php echo count($camposTabela);?>">
			<p>Empresa: <input type="text" name="empresa" id="empresa" size="50" <?php print('value="' . $empresa . '"');?> maxLength="60" required /> </p>
			<p>Nome completo: <input type="text" name="nome" id="nome" size="75" <?php print('value="' . $nome . '"');?> maxLength="120" required /> </p>
			<p>Usuário do AD: <input type="text" name="usuarioAD" id="usuarioAD" size="75" <?php print('value="' . $usuarioAD . '"');?> maxLength="120" required /> </p>
			<p>CPF: <input type="text" name="cpf" id="cpf" size="50" <?php print('value="' . $cpf . '"');?> maxLength="11" required /> </p>
			<p>Data: <input type=text name="data" id="data" <?php print('value="' . $data . '"');?> maxLength="10" required /> </p>
			<?php if($jaDevolvido == 1): ?>
				<label><input type="checkbox" name="jaDevolvido" id="jaDevolvido" checked />Já devolvido</label>
			<?php else: ?>
				<label><input type="checkbox" name="jaDevolvido" id="jaDevolvido" />Já devolvido</label>
			<?php endif; ?>
			<h4>Tipo de Termo:</h4>
			<?php if($tipoTermo == "norm"):?>
				<label><input type="radio" name="tipoTermo" value="norm" required checked />Termo de Responsabilidade</label><br/>
				<label><input type="radio" name="tipoTermo" value="empr"/>Termo de Empréstimo</label><br/>
			<?php else:?>
				<label><input type="radio" name="tipoTermo" value="norm" required />Termo de Responsabilidade</label><br/>
				<label><input type="radio" name="tipoTermo" value="empr" checked />Termo de Empréstimo</label><br/>
			<?php endif;?>
			<br/>
			<table width="100%" border="1">
				<tr>
					<td width="8%"><p>Cabeçalho</p></td>
					<?php for($i = 0;$i < count($camposTabela);$i++){?>
						<td><input type="text" name="cab<?php echo $i+1;?>" id="cab<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(256,count($camposTabela))-count($camposTabela);?>" value="<?php echo $camposTabela[$i]; ?>" required /></td>
					<?php } ?>
				</tr>
				<tr>
					<td width="8%"><p>Equipamento 1</p></td>
					<?php for($i = 0;$i < count($camposTabela);$i++){?>
						<td><input type="text" name="equip1_<?php echo $i+1;?>" id="equip1_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" value="<?php echo $equipamento1[$i]; ?>" required /></td>
					<?php } ?>
				</tr>
				<tr>
					<td width="8%"><p>Equipamento 2</p></td>
					<?php if($equipamento2[0] != ""): ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip2_<?php echo $i+1;?>" id="equip2_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" value="<?php echo $equipamento2[$i]; ?>" /></td>
						<?php } ?>
					<?php else: ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip2_<?php echo $i+1;?>" id="equip2_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" /></td>
						<?php } ?>
					<?php endif; ?>
				</tr>
				<tr>
					<td width="8%"><p>Equipamento 3</p></td>
					<?php if($equipamento3[0] != ""): ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip3_<?php echo $i+1;?>" id="equip3_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" value="<?php echo $equipamento3[$i]; ?>" /></td>
						<?php } ?>
					<?php else: ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip3_<?php echo $i+1;?>" id="equip3_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" /></td>
						<?php } ?>
					<?php endif; ?>
				</tr>
				<tr>
					<td width="8%"><p>Equipamento 4</p></td>
					<?php if($equipamento4[0] != ""): ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip4_<?php echo $i+1;?>" id="equip4_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" value="<?php echo $equipamento4[$i]; ?>" /></td>
						<?php } ?>
					<?php else: ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip4_<?php echo $i+1;?>" id="equip4_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" /></td>
						<?php } ?>
					<?php endif; ?>
				</tr>
				<tr>
					<td width="8%"><p>Equipamento 5</p></td>
					<?php if($equipamento5[0] != ""): ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip5_<?php echo $i+1;?>" id="equip5_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" value="<?php echo $equipamento5[$i]; ?>" /></td>
						<?php } ?>
					<?php else: ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip5_<?php echo $i+1;?>" id="equip5_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" /></td>
						<?php } ?>
					<?php endif; ?>
				</tr>
				<tr>
					<td width="8%"><p>Equipamento 6</p></td>
					<?php if($equipamento6[0] != ""): ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip6_<?php echo $i+1;?>" id="equip6_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" value="<?php echo $equipamento6[$i]; ?>" /></td>
						<?php } ?>
					<?php else: ?>
						<?php for($i = 0;$i < count($camposTabela);$i++){?>
							<td><input type="text" name="equip6_<?php echo $i+1;?>" id="equip6_<?php echo $i+1;?>" size="25" maxLength="<?php echo intdiv(280,count($camposTabela))-count($camposTabela);?>" /></td>
						<?php } ?>
					<?php endif; ?>
				</tr>
			</table>
			
			<p>Descrição por extenso: <input type="text" name="descricao" id="descricao" size="150" <?php print('value="' . $descricao . '"');?> maxLength="360" required /> </p>
			
			<h4>Anexos:</h4>
			<p>Selecione o(s) anexo(s) a excluir:</p>
			<?php verificarAnexos($numTermo);?>
			<input type="submit" name="enviar" id="enviar" value="Editar" />
		</form>
		
		<?php else: ?>
		<p>Termo não encontrado!<br/><a href="editar.php">Volte</a> e digite outro termo.</p>
		
		<?php endif; ?>
		<?php include "rod.php";?>
	</body>
</html>
