# GERADOR DE TERMOS 1.2.1
Um aplicativo online para geração de termos de responsabilidade e termos de devolução, desenvolvido em PHP com uso de MySQL para armazenamento dos dados.
Também é possível anexar arquivos (como termos assinados) em termos e acessá-los em outros momentos.

## REQUERIMENTOS
- PHP 7 (ou superior)
- MySQL 8 (ou superior)
- Apache2
