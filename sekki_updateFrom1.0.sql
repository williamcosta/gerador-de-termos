/*
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
*/

/* SELECIONAR O BANCO DE DADOS */
USE sekki;

/* ADICIONA OS NOVOS CAMPOS A TABELA "TERMOS" */
ALTER TABLE `termos` ADD `usuarioAD` VARCHAR(120) NOT NULL AFTER `data`, ADD `tipoTermo` VARCHAR(4) NOT NULL AFTER `usuarioAD`, ADD `jaDevolvido` INT NOT NULL AFTER `tipoTermo`;

/* SETA TODOS OS TERMOS ANTERIORES COMO "NORMAL" */
UPDATE `termos` SET `tipoTermo`="norm" WHERE 1;

/* SETA TODOS OS TERMOS ANTERIORES COMO "NÃO DEVOLVIDOS" */
UPDATE `termos` SET `jaDevolvido`=0 WHERE 1;
