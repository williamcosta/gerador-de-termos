<?php
/*
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
*/

// Recebe o nº do termo
$numTermo = $_POST['numTermo'];

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["arquivo"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
?>
<!doctype html>
<html>
	<head>
		<title>Anexar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Anexar Arquivos</h1>
		<?php
		include "funcao.php";
		
		// Verifica se o termo existe
		if(ProcurarTermo(true,"numTermo",$numTermo)->num_rows == 0){
			echo "O número de termo informado não foi encontrado!<br/>";
			$uploadOk = 0;
		}
		
		// Verifica se já atingiu o limite de anexos
		if(retornaNumeroAnexos($numTermo) >= 3){
			echo "O termo informado já possui o número máximo de anexos (3)!<br/>";
			$uploadOk = 0;
		}
		
		// Verifica se já existe um arquivo igual
		if (file_exists($target_file)) {
		  print("Já existe um anexo com esse nome!<br/>");
		  $uploadOk = 0;
		}

		// Allow certain file formats
		if($imageFileType != "pdf" && $imageFileType != "jpg" && $imageFileType != "png") {
			print("Só é possível realizar upload de arquivos PDF, JPG e PNG.<br/>");
			$uploadOk = 0;
		}
				
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			echo "Não foi possível anexar o arquivo.";
			// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file($_FILES["arquivo"]["tmp_name"], $target_file)) {
				print("O arquivo '". basename( $_FILES["arquivo"]["name"]). "' foi anexado ao termo nº" . $numTermo . "<br/>");
				
				AnexarArquivo($_FILES["arquivo"]["name"],$numTermo);
				if(isset($_POST['jaDevolvido'])){
					echo "<br/>";
					setDevolvido($numTermo);
				}
			} else {
				echo "Ocorreu um erro ao anexar o arquivo.";
			}
		}
		?>
		<?php include "rod.php";?>
	</body>
</html>
