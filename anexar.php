<!--
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
!-->
<!doctype html>
<html>
	<head>
		<title>Anexar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Anexar arquivos</h1>
		<form action="enviarArquivo.php" method="post" enctype="multipart/form-data">
			<input type="file" name="arquivo" id="arquivo" required /><br/>
			<label>Anexar ao termo: <input type="text" name="numTermo" id="numTermo" required /></label><br/>
			<label><input type="checkbox" name="jaDevolvido" id="jaDevolvido" />Marcar como devolvido</label><br/>
			<input type="submit" value="Enviar" name="submit" />
		</form>
		<?php include "rod.php";?>
	</body>
</html>
