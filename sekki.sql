/*
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
*/

/* CRIA A DATABASE SEKKI */
CREATE DATABASE sekki;

/* ATIVA PARA USAR A DATABASE CRIADA */
USE sekki;

/* CRIAR USUÁRIO PARA TRABALHO COM O BD */
CREATE USER 'usuario'@'%' IDENTIFIED WITH mysql_native_password BY '123';GRANT SELECT, INSERT, UPDATE, DELETE ON *.* TO 'usuario'@'%';ALTER USER 'usuario'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0; 

/* CRIAR TABELAS */
CREATE TABLE IF NOT EXISTS `anexos` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAnexo` varchar(256) NOT NULL,
  `termo` int(11) NOT NULL,
  PRIMARY KEY (`cod`)
);

CREATE TABLE IF NOT EXISTS `termos` (
  `numTermo` int(11) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(60) CHARACTER SET utf8mb4 NOT NULL,
  `nome` varchar(120) CHARACTER SET utf8mb4 NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `camposTabela` varchar(256) NOT NULL,
  `equipamento1` varchar(280) CHARACTER SET utf8mb4 NOT NULL,
  `equipamento2` varchar(280) CHARACTER SET utf8mb4 NOT NULL,
  `equipamento3` varchar(280) CHARACTER SET utf8mb4 NOT NULL,
  `equipamento4` varchar(280) CHARACTER SET utf8mb4 NOT NULL,
  `equipamento5` varchar(280) CHARACTER SET utf8mb4 NOT NULL,
  `equipamento6` varchar(280) CHARACTER SET utf8mb4 NOT NULL,
  `descricao` varchar(360) CHARACTER SET utf8mb4 NOT NULL,
  `data` varchar(10) NOT NULL,
  `usuarioAD` varchar(120) CHARACTER SET utf8mb4 NOT NULL,
  `tipoTermo` varchar(4) NOT NULL,
  `jaDevolvido` int(11) NOT NULL,
  PRIMARY KEY (`numTermo`)
);
