<!--
Gerador de Termos 1.2.1

Copyright © 2020, William Alcântara Costa
Todos os direitos reservados.
Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
!-->
<!doctype html>
<html>
	<head>
		<title>Editar - Gerador de Termos</title>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body>
		<?php include "cab.php";?>
		<h1>Editar Termo</h1>
		<form id="pesquisaEdita" name="pesquisaEdita" method="get" action="editarTermo.php">
			<p>Número do termo: <input type="text" name="numTermo" id="numTermo" size="15" required />
			<input type="submit" value="Editar"/></p>
		</form>
		<?php include "rod.php";?>
	</body>
</html>
