<?php
	/*
	Gerador de Termos 1.2.1

	Copyright © 2020, William Alcântara Costa
	Todos os direitos reservados.
	Esse código está licenciado sob a Licença de Três Cláusulas BSD - disponível em https://gitlab.com/williamcosta/gerador-de-termos/-/blob/master/LICENSE
	*/
	
	//Recebendo dados
	$numTermo = $_GET['numTermo'];
	$isDevol = $_GET['isDevol'];
	
	function adicionarLinha($array){
		print("<tr>");
		$linha = explode(";",$array);
		for($i = 0;$i < count($linha);$i++){
			print("<td>" . $linha[$i] . "</td>");
		}
		print("</tr>");
	}
	
	include "funcao.php";
		
	// COMEÇO DA PÁGINA TERMO.PHP
	$termoEncontrado = false;
	if(strcmp(LerTermo($numTermo,true),"0 resultados")){
		// TERMO ENCONTRADO
		
		$row = LerTermo($numTermo,false)->fetch_assoc();
		$empresa = $row["empresa"];
		$nome = $row["nome"];
		$cpf = $row["cpf"];
		$camposTabela = $row['camposTabela'];
		$equipamento1 = $row['equipamento1'];
		$equipamento2 = $row['equipamento2'];
		$equipamento3 = $row['equipamento3'];
		$equipamento4 = $row['equipamento4'];
		$equipamento5 = $row['equipamento5'];
		$equipamento6 = $row['equipamento6'];
		$descricao = $row['descricao'];
		$data = $row['data'];
		$tipoTermo = $row['tipoTermo'];
		
		$termoEncontrado = true;
	} else {
		// TERMO **NÃO** ENCONTRADO
		
		$termoEncontrado = false;
	}
?>
<!doctype html>
<html>
	<head>
		<?php if($isDevol == 0): ?>
			<title>Termo de Responsibilidade</title>
		<?php else: ?>
			<title>Termo de Devolução</title>
		<?php endif; ?>
		<link rel="stylesheet" href="estilo.css">
	</head>
	<body class="corpoTermo">
		<?php
		if($termoEncontrado){
		?>
		<table width="100%" border="0">
			<tr>
				<td width="15%"><img src="img/logotipo.png" alt="(E) EMPRESA" witdh="262" height="98"/></td>
				<?php if($isDevol == 0 and $tipoTermo == "norm"):?>
					<td width="85%"><h2 class="centro">TERMO DE RESPONSABILIDADE PARA AQUISIÇÃO E UTILIZAÇÃO DE EQUIPAMENTOS</h2></td>
				<?php elseif($isDevol == 0 and $tipoTermo == "empr"):?>
					<td width="85%"><h2 class="centro">TERMO DE RESPONSABILIDADE PARA EMPRÉSTIMO DE EQUIPAMENTO</h2></td>
				<?php else: ?>
					<td width="85%"><h2 class="centro">TERMO DE DEVOLUÇÃO DE EQUIPAMENTOS</h2></td>
				<?php endif; ?>
			</tr>
		</table>
		<h3 class="direita"><?php print($empresa);?></h3><hr/>
		
		<?php if($isDevol == 0 and $tipoTermo == "norm"): ?>
		<p class="alinhado">Eu, <?php print($nome);?>, inscrito no CPF: <?php print($cpf);?> funcionário(a) da empresa <?php print($empresa);?> declaro para os devidos fins que estou recebendo os equipamentos descritos abaixo, nas devidas condições legais que regem a legislação da CLT.</p>
		
		<?php elseif($isDevol == 0 and $tipoTermo == "empr"): ?>
		<p class="alinhado">Eu, <?php echo $nome;?>, portador(a) do CPF: <?php echo $cpf;?> assumo a responsabilidade plena e integra para utilizar o(s) equipamento(s) abaixo. Me comprometo a devolvê-lo(s) nas mesmas condições em que me foi(ram) entregue(s).
		
		<?php endif; ?>
		
		<?php if($isDevol == 0):?>
		<table width="100%" border="1">
			<tr>
				<?php
					// TABELA DOS EQUIPAMENTOS
					$cab = explode(";",$camposTabela);
					for($i = 0;$i < count($cab);$i++){
				?>
				<td class="cabecalho">
					<?php
						print($cab[$i]);
					?>
				</td>
				<?php
					}
				?>
			</tr>
			<tr>
				<?php
					$linha = explode(";",$equipamento1);
					for($i = 0;$i < count($linha);$i++){
				?>
				<td>
					<?php
						print($linha[$i]);
					?>
				</td>
				<?php
					}
				?>
			</tr>
			<?php
				if(!empty($equipamento2)){
					adicionarLinha($equipamento2);
				}
				if(!empty($equipamento3)){
					adicionarLinha($equipamento3);
				}
				if(!empty($equipamento4)){
					adicionarLinha($equipamento4);
				}
				if(!empty($equipamento5)){
					adicionarLinha($equipamento5);
				}
				if(!empty($equipamento6)){
					adicionarLinha($equipamento6);
				}
			?>
		</table>
		<?php endif; ?>
		
		<?php if($isDevol == 0 and $tipoTermo == "norm"):?>
		<p class="alinhado">O equipamento descrito acima contém <?php print($descricao);?>.<br/>
		O objeto do presente contrato é a disponibilização ao funcionário, da <?php print($empresa);?> de uma estação móvel concedido pela empresa. O funcionário obriga-se durante o período de permanência dos equipamentos, a manter a estação em perfeito estado de uso e conservação, responsabilizando-se pelo uso correto em caso de danos por mau uso e pagamento de todas as despesas de manutenção decorrente dos danos causados ao aparelho por mau uso, perda ou roubo dos equipamentos.</p>
		<ul>
			<li>Instalar apenas aplicativos com licença de livre distribuição, ou que a empresa venha adquirir;</li>
			<li>Não copiar, reproduzir ou distribuir documentos, arquivos ou programas que forem de direito da empresa;</li>
			<li>Após o uso todos equipamentos descritos acima devem ser devolvidos exclusivamente para a T.I assinando o termo de baixa.</li>
		</ul>
		
		<?php endif;?>
		
		<?php if($isDevol == 0):?>
		<p class="direita">Porto Alegre, <?php print($data);?></p><br/><br/>
		<?php endif;?>
		
		<?php if($isDevol == 1):?>
		
			<p class="alinhado">Eu, <?php echo $nome; ?>, inscrito no CPF: <?php echo $cpf; ?>, funcionário(a) da empresa <?php echo $empresa; ?> declaro para os devidos fins que estou devolvendo os equipamentos descritos no termo de Nº <?php echo $numTermo; ?>.</p>
			<p class="direita">Porto Alegre, <?php echo date("d/m/Y");?></p><br/><br/>
			
		<?php endif; ?>
		
		
		<table width="100%" border="0">
			<tr>
				<td width="50%"><hr/><p class="centro"><?php print($empresa);?></p></td>
				<td width="50%"><hr/><p class="centro"><?php print($nome);?></p></td>
			</tr>
		</table>
		<p>Termo nº<?php print($numTermo);?></p>
		<?php
		} else {
			include "cab.php";
		?>
		<h2 class="centro">TERMO NÃO ENCONTRADO!</h2>
		<p class="centro">Volte a <a href="procura.php">página anterior</a> e procure por outro número de termo</p>
		<?php
			include "rod.php";
		}
		?>
	</body>
</html>
